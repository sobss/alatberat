<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Megatron - Multipurpose Responsive HTML5 Template</title>
    <meta name="description" content="Megatron - Multipurpose Responsive HTML5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="vendors/animate.css/animate.min.css">
    <link href="//fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/megatron/styles.css">
    <!--Main stylesheet-->
    <link rel="stylesheet" href="assets/css/main.css">
    <!--Modernizr js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
    <p class="browsenrupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!--Page Content-->
    <div class="page-content">
      <!-- Page Navigation-->
      <div class="main-nav small-nav bgc-transparent fixed-tranformed-bg-light nav-fixed large-container">
        <div class="container">
          <div class="nav-content-wrapper">
            <div class="pull-left">
              <div class="megatron inline">
                <div class="cell-vertical-wrapper">
                  <div class="cell-middle"><a href="index.php">
                      <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                      </div>
                      <div class="brand">MEGATRON</div></a></div>
                </div>
              </div>
            </div>
            <div class="pull-right visible-lg">
              <nav class="os-menu main-menu">
                <ul>
                  <li><a href="index.php">HOME</a>
                  </li>
                  <li><a href="about.php">ABOUT US</a>
                  </li>
                  <li><a href="category.php">RENTAL</a>
                  </li>
                  <li><a href="category.php">SALE</a>
                  </li>
                  <li><a href="contact.php">CONTACT US</a>
                  </li>
                </ul>
              </nav>
              <div class="nav-function nav-item">
                <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                </div>
                <div class="__offcanvas-button"><a href="#" class="offcanvas-nav-toggle icon-menu27"></a></div>
              </div>
            </div>
            <div class="pull-right nav-item hidden-lg"><a href="shop-cart.html" class="cart-button icon-shopping111"></a><a href="#" class="mobile-nav-toggle nav-hamburger"><span></span></a></div>
          </div>
        </div>
      </div>
      <!-- Page Navigation-->
      <!--Page Header-->
      <header class="page-section header-portfolio-spcial-2 no-padding-bottom">
        <div class="container text-center">
          <div class="row">
            <div class="col-xs-12 section-block"><a href="index.php" class="megatron size-2">
                <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                </div>
                <div class="brand">MEGATRON</div></a>
            </div>
            <div class="col-xs-12 section-block">
              <ul class="social circle">
                <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
              </ul>
            </div>
            <div class="col-md-8 col-md-offset-2 section-block-p">
              <p>Megatron is a Multipurpose HTML 5 Template with which responds to the most demanding customers. It can be a great choice for your Corporate, Ecommerce, Portfolio, Creative Agency etc. This theme can easily satisfy all of your needs. Use Megatron template to create any interactive website you want.</p>
            </div>
          </div>
        </div>
      </header>
      <!--End Page Header-->
      <!--Page Body-->
      <div id="page-body" class="page-body">
        <section class="page-section bgc-light">
          <div class="container"><img src="assets/images/other/about-us-large-img.jpg" alt="about us" class="img-responsive"><br><br>
            <div class="row">
              <div class="col-md-4 section-block">
                <h4>OUR FAVORITE INSPIRATIONAL</h4>
                <q>Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do. If you haven't found it yet, keep looking. Don't settle. As with all matters of the heart, you'll know when you find it.</q>
                <h6 class="color-primary pull-right">- STEVE JOBS -</h6>
              </div>
              <div class="col-md-8 section-block">
                <h4>OUR HISTORY</h4>
                <p>Megatron Creative is an award winning digital media, web design and PR agency. In 2012, Megatron Creative was recognized as one of the top 3 Advertising Agencies in Wisconsin by readers of Corporate Report Wisconsin. The company recently received two gold Hermes Awards for design and five Alchemy Awards from the Public Relations Society of America, more than any other agency in Madison, Wisconsin.</p>
                <p>Megatron Creative was also a 2006 Wisconsin Better Business Bureau Business Ethics Torch award finalist and has been recognized twice by Madison Magazine as one of the best places to work in Dane County.</p>
              </div>
            </div>
          </div>
        </section>
        <section class="page-section bgc-gray-lighter">
          <div class="counter-group">
            <div class="container">
              <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                <div class="block-counter text-center">
                  <h1 data-from="0" data-to="3195" class="timer mb-15 hr-heading hr-primary">0</h1>
                  <p class="fz-3 font-serif-italic mb-0">Projects Finished</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                <div class="block-counter text-center">
                  <h1 data-from="0" data-to="4638" class="timer mb-15 hr-heading hr-primary">0</h1>
                  <p class="fz-3 font-serif-italic mb-0">Happy Clients</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                <div class="block-counter text-center">
                  <h1 data-from="0" data-to="3690" class="timer mb-15 hr-heading hr-primary">0</h1>
                  <p class="fz-3 font-serif-italic mb-0">Hours of work</p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                <div class="block-counter text-center">
                  <h1 data-from="0" data-to="3468" class="timer mb-15 hr-heading hr-primary">0</h1>
                  <p class="fz-3 font-serif-italic mb-0">Cup Of Coffee</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="page-section">
          <div class="container text-center mb-50">
            <header class="hr-header">
              <h2 class="smb">OUR TEAM</h2>
              <p class="common-serif __caption">Meet our management team</p>
              <div class="separator-2-color"></div>
            </header>
            <div class="row">
              <div class="col-lg-8 col-md-10 col-xs-12 col-lg-offset-2 col-md-offset-1">
                <p>Most businesses think that product is the most important thing, but without great leadership, mission and a team that deliver results at a high level, even the best product won't make a company successful.</p>
              </div>
            </div>
          </div>
          <div class="team">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                  <div class="block-team">
                    <div class="__image">
                      <div><a href="page-about-me.html"><img src="assets/images/team/team-member-1.jpg" alt="team member"/></a></div>
                    </div>
                    <div class="__info">
                      <h4 class="__name"><a href="page-about-me.html">GEORGE</a></h4>
                      <p class="__role font-serif-italic">Ceo / Co-Founder</p>
                      <p class="__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet </p>
                      <ul class="social circle primary">
                        <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                  <div class="block-team">
                    <div class="__image">
                      <div><a href="page-about-me.html"><img src="assets/images/team/team-member-2.jpg" alt="team member"/></a></div>
                    </div>
                    <div class="__info">
                      <h4 class="__name"><a href="page-about-me.html">PATRICIA</a></h4>
                      <p class="__role font-serif-italic">Vice Director</p>
                      <p class="__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet </p>
                      <ul class="social circle primary">
                        <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                  <div class="block-team">
                    <div class="__image">
                      <div><a href="page-about-me.html"><img src="assets/images/team/team-member-3.jpg" alt="team member"/></a></div>
                    </div>
                    <div class="__info">
                      <h4 class="__name"><a href="page-about-me.html">BARBARA</a></h4>
                      <p class="__role font-serif-italic">HR Manager</p>
                      <p class="__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet </p>
                      <ul class="social circle primary">
                        <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 section-block">
                  <div class="block-team">
                    <div class="__image">
                      <div><a href="page-about-me.html"><img src="assets/images/team/team-member-4.jpg" alt="team member"/></a></div>
                    </div>
                    <div class="__info">
                      <h4 class="__name"><a href="page-about-me.html">RICHARD</a></h4>
                      <p class="__role font-serif-italic">Head Marketing</p>
                      <p class="__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet </p>
                      <ul class="social circle primary">
                        <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                        <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <!--End Page Body-->
      <!--Page Footer-->
      <footer class="page-footer">
        <div class="footer-foot-1 bgc-gray-darkest">
          <div class="container text-center">
            <p class="font-heading fz-6-s">MEGATRON &copy; 2015 OSTHEMES - ENVATO PTY LTD</p>
          </div>
        </div>
      </footer>
      <!--End Page Footer-->

      

      <div id="shop-quickview" style="display:none;" class="shop-quick-view clearfix">
        <div class="row">
          <div class="col-sm-6 col-xs-12 product-thumbnail-slider-wrapper __content-left">
            <div class="product-thumbnail-slider">
              <div class="product-syn-slider-1-wrapper">
                <div class="product-syn-slider-1 syn-slider-1 direction-nav">
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-2-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-2-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-4-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-4-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-6-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-6-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-11-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-11-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-13-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-13-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-14-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-14-l.jpg" alt="Product slider image"/></div>
                  </div>
                </div>
              </div>
              <div class="product-syn-slider-2-wrapper">
                <div class="product-syn-slider-2 syn-slider-2">
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-2-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-4-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-6-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-11-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-13-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-14-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12 __content-right">
            <div class="product-detail">
              <h3 class="fz-3-l"><a href="shop-single-fullwidth.html">FITCH ZIP THROUGH HOODIE</a></h3>
              <div class="__rating clearfix">
                <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                <p class="color-secondary">(1 customer review)</p>
              </div>
              <div class="__price fz-4 font-heading">
                <del>$500</del><span class="color-primary">$300</span>
              </div>
              <div class="__text">
                <p>Nullam fringilla tristique elit id varius. Nulla lacinia quam nec venenatis dignissim. Vivamus volutpat tempus semper. Cras feugiat mi sit amet risus consectetur, non consectetur nisl finibus. Ut ac eros quis mi volutpat cursus vel non risus. In non neque lacinia, aliquet tortor sed, consectetur nibh. Nulla faucibus risus in ligula elementum bibendum.</p>
              </div>
              <form>
                <div class="__option-2 clearfix">
                  <div class="__quantity">
                    <div class="quantity-input">
                      <input type="text" value="1" class="number"/>
                      <button class="add icon-up-open-mini"></button>
                      <button class="subract icon-down-open-mini"></button>
                    </div>
                  </div>
                  <div class="__button"><a href="shop-cart.html" class="btn btn-primary"><i class="icon-shopping111"></i>ADD TO CART</a></div>
                </div>
              </form>
              <div class="__others">
                <p>SKU:&nbsp;<a href="#">0010</a></p>
                <p>CATEGORY:&nbsp;<a href="#">Other</a></p>
                <p>TAGS:&nbsp;<a href="#">Dress</a>,&nbsp;<a href="#">Fashion</a></p>
              </div>
              <ul class="social circle secondary responsive">
                <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                <li><a href="#"><i class="icon icon-stumbleupon"></i></a></li>
                <li><a href="#"><i class="icon icon-instagrem"></i></a></li>
                <li><a href="#"><i class="icon icon-dribbble-1"></i></a></li>
                <li><a href="#"><i class="icon icon-github"></i></a></li>
                <li><a href="#"><i class="icon icon-vimeo"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Page content-->
    <!--Javascript Library-->
    <button id="back-to-top-btn"><i class="icon-up-open-big"></i></button>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="vendors/waypoints/lib/shortcuts/sticky.min.js"></script>
    <script src="vendors/smoothscroll/SmoothScroll.js"></script>
    <script src="vendors/wow/dist/wow.min.js"></script>
    <script type="text/javascript" src="vendors/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="vendors/jquery-countTo/jquery.countTo.js"></script>
    <script type="text/javascript" src="vendors/slick-carousel/slick/slick.js"></script>
    <script type="text/javascript" src="vendors/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <!-- Google analytics-->
    <script type="text/javascript">(function(b,o,i,l,e,r){b.GoogleAsnalyticsObject=l;b[l]||(b[l]=function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;e=o.createElement(i);r=o.getElementsByTagName(i)[0];e.src='//www.google-analytics.com/analytics.js';r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));ga('create','UA-57387972-3');ga('send','pageview');</script>
    <!--End Javascript Library-->
  </body>
</html>