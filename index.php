<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Alat Berat SBY</title>
    <meta name="description" content="Megatron - Multipurpose Responsive HTML5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <link rel="stylesheet" href="vendors/animate.css/animate.min.css">
    <link href="//fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/megatron/styles.css">
    <!--Main stylesheet-->
    <link rel="stylesheet" href="assets/css/main.css">
    <!--Modernizr js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
    <p class="browsenrupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!--Page Content-->
    <div class="page-content">
      <!-- Page Navigation-->
      <div class="main-nav bgc-transparent nav-absolute large-container ">
        <div class="__middle">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline logo-light">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.php">
                        <div class="logo">
                        <!-- <img src="assets/images/logo/he.png" alt=""> -->
                        <svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                        </div>
                        <div class="brand"></div>MEGATRON</a></div>
                  </div>
                </div>
              </div>
              <div class="pull-right">
                <nav class="os-menu  main-menu text-center">
                  <ul>
                    <li><a href="index.php">HOME</a>
                    </li>
                    <li><a href="about.php">ABOUT US</a>
                    </li>
                    <li><a href="category.php">RENTAL</a>
                    </li>
                    <li><a href="category.php">SALE</a>
                    </li>
                    <li><a href="contact.php">CONTACT US</a>
                    </li>
                  </ul>
                </nav>

                <div class="nav-function nav-item">
                  <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                  </div>
                  <div class="__offcanvas-button"><a href="#" class="offcanvas-nav-toggle icon-menu27"></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="main-nav small-nav bgc-transparent fixed-tranformed-bg-light nav-fixed large-container">
        <div class="container">
          <div class="nav-content-wrapper">
            <div class="pull-left">
              <div class="megatron inline">
                <div class="cell-vertical-wrapper">
                  <div class="cell-middle"><a href="index.php">
                      <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                      </div>
                      <div class="brand">MEGATRON</div></a></div>
                </div>
              </div>
            </div>
            <div class="pull-right visible-lg">
              <nav class="os-menu main-menu">
                <ul>
                  <li><a href="index.php">HOME</a>
                  </li>
                  <li><a href="about.php">ABOUT US</a>
                  </li>
                  <li><a href="category.php">RENTAL</a>
                  </li>
                  <li><a href="category.php">SALE</a>
                  </li>
                  <li><a href="contact.php">CONTACT US</a>
                  </li>
                </ul>
              </nav>
              <div class="nav-function nav-item">
                <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                </div>
                <div class="__offcanvas-button"><a href="#" class="offcanvas-nav-toggle icon-menu27"></a></div>
              </div>
            </div>
            <div class="pull-right nav-item hidden-lg"><a href="shop-cart.html" class="cart-button icon-shopping111"></a><a href="#" class="mobile-nav-toggle nav-hamburger"><span></span></a></div>
          </div>
        </div>
      </div>
      
      <nav class="offcanvas-nav bgc-gray-darkest"><a href="#" class="offcanvas-nav-toggle"><i class="icon-wrong6"></i></a>
        <h6 class="size-s smb">CUSTOM PAGES</h6>
        <nav class="nav-single nav-item">
          <ul>
            <ul>
              <li><a href="index.php">HOME</a>
              </li>
              <li><a href="about.php">ABOUT US</a>
              </li>
              <li><a href="category.php">RENTAL</a>
              </li>
              <li><a href="category.php">SALE</a>
              </li>
              <li><a href="contact.php">CONTACT US</a>
              </li>
            </ul>
          </ul>
        </nav>

        <h6 class="size-s">GALLERY</h6>
        <div class="widget widget-gallery">
          <ul class="__widget-content magnific-gallery">
            <li><a href="assets/images/vintage/vintage-1.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-1-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-2.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-2-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-3.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-3-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-4.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-4-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-5.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-5-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-6.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-6-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-7.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-7-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-8.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-8-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-9.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-9-thumbnail.jpg" alt="instagram image"/></a></li>
          </ul>
        </div>
        <ul class="social circle gray">
          <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
          <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
          <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
          <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
        </ul>
      </nav>

      <div class="mobile-nav"><a href="#" class="mobile-nav-toggle"><i class="icon-close47"></i></a><a href="index.php" class="megatron">
          <div class="logo">
            <svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
          </div>
          <div class="brand">MEGATRON</div></a>
        <nav class="os-menu mobile-menu">
          <ul>
            <li><a href="index.php">HOME</a>
            </li>
            <li><a href="about.php">ABOUT US</a>
            </li>
            <li><a href="category.php">RENTAL</a>
            </li>
            <li><a href="category.php">SALE</a>
            </li>
            <li><a href="contact.php">CONTACT US</a>
            </li>
          </ul>
        </nav>
        <div class="search-area">
          <form method="post" class="search-box">
            <input type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-icon"></i></button>
          </form>
        </div>
        <div class="social-area">
          <ul class="social circle gray">
            <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
            <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
            <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
            <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
          </ul>
        </div>
      </div>
      <!-- Page Navigation-->
      <!--Page Header-->
      <header class="page-header home home-slider-1">
        <div class="slider caption-slider control-nav">
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/background/background-101.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-7">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-2 text-center">
                    <div class="container">
                      <div class="caption-wrapper"><a href="index.php" class="megatron logo-light size-3 caption">
                          <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                          </div></a>
                        <h1 class="text-responsive size-ll caption">WELCOME TO <br/> COMPANY NAME</h1>
                        <p class="font-serif-italic fz-3 caption">We can create a brand that stands out and truly reflects your business</p>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/background/background-102.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-3">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-2 text-center">
                    <div class="container">
                      <div class="caption-wrapper"><a href="index.php" class="megatron logo-light size-3 caption">
                          <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                          </div></a>
                        <h1 class="text-responsive size-ll caption">CREATIVITY HAS NO <br/> BOUNDARIES</h1>
                        <p class="font-serif-italic fz-3 caption">Welcome to Company Name & Vision</p>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets/images/background/background-103.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-5">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-2 text-center">
                    <div class="container">
                      <div class="caption-wrapper"><a href="index.php" class="megatron logo-light size-3 caption">
                          <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                          </div></a>
                        <h1 class="text-responsive size-ll caption">BRANDING,WEBSITE<br/> & MARKETING</h1>
                        <p class="font-serif-italic fz-3 caption">We will work with you to fully understand your business and your target</p>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!--End Page Header-->



      <!--Page Body-->
      <div id="page-body" class="page-body">
        
        <!-- Sekilas tentang perusahaan -->
        <section class="page-section bgc-light supper-padding supper-padding-top">
          <div class="container text-center">
            <div class="col-md-8 col-md-offset-2">
              <header class="hr-header section-header">
                <h2 class="smb">COMPANY NAME</h2>
                <p class="common-serif __caption">Company Vision</p>
                <div class="separator-2-color"></div>
                <p class="__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

              </header>
              <div class="caption-wrapper">
                <div class="__buttons caption"><a href="#" class="btn btn-primary">READ MORE</a><br/></div>
              </div>
            </div>
          </div>
        </section>
        <!-- Akhir Sekilas tentang perusahaan -->
        
        <section class="page-section no-padding">
          <div class="call-to-action-common bgc-dark-o-5">
            <div class="container">
              <div class="__content-wrapper row">
                <div class="col-lg-10 col-md-9 col-xs-12 __content-left">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle">
                      <p class="font-serif-italic mb-0 fz-3 color-dark">Like what you see? Start now with this demo or check out our other demos to find what you need.</p>
                    </div>
                  </div>
                </div>
                <div data-wow-delay="0.3s" class="col-lg-2 col-md-3 col-xs-12 __content-right wow fadeInRight">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.php" class="fullwidth btn-border btn-light">CHECK IT OUT</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- produk -->
        <section class="page-section bgc-light">
          <div class="group-portfolio isotope-container">

            <div class="container text-center">
              <div class="col-md-8 col-md-offset-2">
                <header class="hr-header section-header">
                  <p class="common-serif __caption">Company Vision</p>
                  <div class="separator-2-color"></div>
                </header>
              </div>
            </div>

            <div class="isotope-grid remove-gutter container">
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 graphic-design social-marketing web-design">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/1.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/1.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 branding seo-optimization graphic-design">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/2.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/2.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 social-marketing content-marketing branding">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/3.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/3.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 seo-optimization web-design social-marketing">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/4.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/4.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
            </div>

            <div class="caption-wrapper text-center">
              <div class="__buttons caption"><a href="#" class="btn btn-primary">SEE MORE</a><br/></div>
            </div>

            <hr class="hr-3 hr-gray-border">

            <div class="container text-center">
              <div class="col-md-8 col-md-offset-2">
                <header class="hr-header section-header">
                  <p class="common-serif __caption">Company Vision</p>
                  <div class="separator-2-color"></div>
                </header>
              </div>
            </div>

            <div class="isotope-grid remove-gutter container">
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 graphic-design social-marketing web-design">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/1.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/1.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 branding seo-optimization graphic-design">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/2.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/2.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 social-marketing content-marketing branding">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/3.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/3.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
              <div class="grid-item col-md-3 col-sm-6 col-xs-12 seo-optimization web-design social-marketing">
                <div class="block-portfolio title-foot zoom">
                  <div class="overlay-container"><img src="assets/images/product/4.jpg" alt="BY THE SEA " class="__image"/>
                    <div class="overlay bgc-dark-o-7">
                      <div class="cell-vertical-wrapper">
                        <div class="cell-middle">
                        </div>
                      </div><a href="assets/images/product/4.jpg" class="__zoom zoom-button"><i class="icon-resize-full"></i></a>
                    </div>
                  </div>
                  <footer>
                    <h4 class="__title">BY THE SEA </h4>
                    <div class="__caption font-serif-italic">Lorem ipsum dolor sit amet</div>
                  </footer>
                </div>
              </div>
            </div>

            <div class="caption-wrapper text-center">
              <div class="__buttons caption"><a href="#" class="btn btn-primary">SEE MORE</a><br/></div>
            </div>
            
          </div>
        </section>
        <!-- akhir produk -->
        
      </div>
      <!--End Page Body-->


      <!--Page Footer-->
      <footer id="footer" class="page-footer footer-preset-3">
        <div class="footer-body bgc-gray-darker">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <div class="footer-widget-about"><a href="index.html" class="megatron inline size-2">
                    <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                    </div>
                    <div class="brand">MEGATRON</div></a>
                  <p>Megatron is a Multipurpose HTML 5 Template with which responds to the most demanding customers. It can be a great choice for your Corporate, Ecommerce, Portfolio, Creative Agency etc. This theme can easily satisfy all of your needs.</p>
                  <ul class="social circle">
                    <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-flickr"></i></a></li>
                    <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-5 col-sm-7 col-xs-12">
                <div class="footer-widget-info">
                  <h4>INFORMATION</h4>
                  <div class="__content font-heading fz-6-ss __2-col">
                    <div><span><a href="#">HOME</a></span><span><a href="#">BLOG</a></span><span><a href="#">CONTACT</a></span><span><a href="#">ABOUT US</a></span><span><a href="#">PROJECTS</a></span></div>
                    <div><span><a href="#">CONSUTING</a></span><span><a href="#">TAX SERVICE</a></span><span><a href="#">SPECIALTY SERVICES</a></span><span><a href="#">TRANSACTION ADVISORY</a></span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-5 col-xs-12">
                <div class="footer-widget-contact">
                  <h4>OUR OFFICE</h4>
                  <div class="__content font-heading fz-6-ss">
                    <div class="__address"><i class="icon icon-location"></i><span>14 Tottenham Road, London, England.</span></div>
                    <div class="__phone"><i class="icon icon-phone-1"></i><span>(123) 45678910</span></div>
                    <div class="__email"><i class="icon icon-paper-plane"></i><span>info@osthemes.com</span></div>
                    <div class="__phone-2"><i class="icon icon-mobile-1"></i><span>(123) 45678910</span></div>
                    <div class="__time"><i class="icon icon-clock-1"></i><span>Mon - Sat: 9:00 - 18:00</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-foot-1 bgc-gray-darkest">
          <div class="container text-center">
            <p class="font-heading fz-6-s">MEGATRON &copy; 2015 OSTHEMES - ENVATO PTY LTD</p>
          </div>
        </div>
      </footer>
      <!--End Page Footer-->


    </div>
    <!--End Page content-->
    <!--Javascript Library-->
    <button id="back-to-top-btn"><i class="icon-up-open-big"></i></button>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="vendors/waypoints/lib/shortcuts/sticky.min.js"></script>
    <script src="vendors/smoothscroll/SmoothScroll.js"></script>
    <script src="vendors/wow/dist/wow.min.js"></script>
    <script src="vendors/parallax.js/parallax.js"></script>
    <script type="text/javascript" src="vendors/jquery-countTo/jquery.countTo.js"></script>
    <script type="text/javascript" src="vendors/slick-carousel/slick/slick.js"></script>
    <script type="text/javascript" src="vendors/isotope/dist/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="vendors/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="vendors/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="vendors/jquery-modal/jquery.modal.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <!-- Google analytics-->
    <script type="text/javascript">(function(b,o,i,l,e,r){b.GoogleAsnalyticsObject=l;b[l]||(b[l]=function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;e=o.createElement(i);r=o.getElementsByTagName(i)[0];e.src='//www.google-analytics.com/analytics.js';r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));ga('create','UA-57387972-3');ga('send','pageview');</script>
    <!--End Javascript Library-->
  </body>
</html>