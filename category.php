<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Megatron - Multipurpose Responsive HTML5 Template</title>
    <meta name="description" content="Megatron - Multipurpose Responsive HTML5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="vendors/animate.css/animate.min.css">
    <link href="//fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/megatron/styles.css">
    <!--Main stylesheet-->
    <link rel="stylesheet" href="assets/css/main.css">
    <!--Modernizr js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  </head>
  <body class="category-fullwidth-page">
    <!--[if lt IE 8]>
    <p class="browsenrupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!--Page Content-->
    <div class="page-content">
      <!-- Page Navigation-->
      <div class="main-nav nav-relative">

        <div class="__middle bgc-light">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.html">
                        <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                        </div>
                        <div class="brand">MEGATRON</div></a></div>
                  </div>
                </div>
              </div>
              <div class="pull-right">
                <nav class="os-menu main-menu text-center">
                  <ul>
                    <li><a href="index.php">HOME</a>
                    </li>
                    <li><a href="about.php">ABOUT US</a>
                    </li>
                    <li><a href="category.php">RENTAL</a>
                    </li>
                    <li><a href="category.php">SALE</a>
                    </li>
                    <li><a href="contact.php">CONTACT US</a>
                    </li>
                  </ul>
                </nav>

                <div class="nav-function nav-item">
                  <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                  </div>
                  <div class="__offcanvas-button"><a href="#" class="offcanvas-nav-toggle icon-menu27"></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="main-nav small-nav bgc-transparent fixed-tranformed-bg-light nav-fixed large-container">
        <div class="container">
          <div class="nav-content-wrapper">
            <div class="pull-left">
              <div class="megatron inline">
                <div class="cell-vertical-wrapper">
                  <div class="cell-middle"><a href="index.php">
                      <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                      </div>
                      <div class="brand">MEGATRON</div></a></div>
                </div>
              </div>
            </div>
            <div class="pull-right visible-lg">
              <nav class="os-menu main-menu">
                <ul>
                  <li><a href="index.php">HOME</a>
                  </li>
                  <li><a href="about.php">ABOUT US</a>
                  </li>
                  <li><a href="category.php">RENTAL</a>
                  </li>
                  <li><a href="category.php">SALE</a>
                  </li>
                  <li><a href="contact.php">CONTACT US</a>
                  </li>
                </ul>
              </nav>
              <div class="nav-function nav-item">
                <div class="__search"><a href="__modal-search.html" data-modal-open="data-modal-open" class="icon-search-icon"></a>
                </div>
                <div class="__offcanvas-button"><a href="#" class="offcanvas-nav-toggle icon-menu27"></a></div>
              </div>
            </div>
            <div class="pull-right nav-item hidden-lg"><a href="shop-cart.html" class="cart-button icon-shopping111"></a><a href="#" class="mobile-nav-toggle nav-hamburger"><span></span></a></div>
          </div>
        </div>
      </div>

      <aside class="shop-category-sidebar shop-sidebar-offcanvas bgc-light"><span class="shop-sidebar-toggle"><i class="icon-wrong6"></i></span>
        <div class="widget widget-search">
          <div class="__widget-content">
            <form method="post" class="search-box">
              <input type="search" placeholder="Search..."/>
              <button type="submit"><i class="icon-search-icon"></i></button>
            </form>
          </div>
        </div>
        <div class="widget widget-price-filter">
          <h6 class="__widget-title">FILTER BY PRICE</h6>
          <div data-from="5" data-to="150" data-min="0" data-max="500" class="__widget-content price-filter">
            <div class="price-slider-range"></div>
            <div class="price-filter-detail">
              <div class="price pull-left">Price :&nbsp;&pound;<span class="from"></span><span class="hyphen">&nbsp;-&nbsp;&pound;</span><span class="to"></span></div><a href="#" class="pull-right btn btn-small btn-primary">FILTER</a>
            </div>
          </div>
        </div>
        <hr class="mb-40"/>
        <div class="widget widget-categories">
          <h6 class="__widget-title">CATEGORIES</h6>
          <ul class="__widget-content">
            <li><a href="#">DESTINATION (8)</a></li>
            <li><a href="#">FOOD (8)</a></li>
            <li><a href="#">LANDSCAPE (8)</a></li>
            <li><a href="#">NATURE (8)</a></li>
            <li><a href="#">PHOTOGRAPHY (8)</a></li>
            <li><a href="#">TRAVEL (8)</a></li>
          </ul>
        </div>
        <div class="widget widget-product">
          <h6 class="__widget-title">RECENT PRODUCTS</h6>
          <div class="__widget-content">
            <div class="block-shop-product-small">
              <div class="__image overlay-container"><img src="assets/images/shop/product-img-1-s.jpg" alt="Shop Product"/>
                <div class="overlay-hover text-center bgc-dark-o-3">
                  <ul>
                    <li class="clearfix"><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="__info">
                <p class="__category font-serif-italic fz-6-s">Women / Clothe</p><a href="#" class="font-heading fz-6-ss">SKATER DRESS IN LEAF</a>
                <div class="clearfix">
                  <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div><span class="__price color-primary font-heading">$280</span>
                </div>
              </div>
            </div>
            <div class="block-shop-product-small">
              <div class="__image overlay-container"><img src="assets/images/shop/product-img-6-s.jpg" alt="Shop Product"/>
                <div class="overlay-hover text-center bgc-dark-o-3">
                  <ul>
                    <li class="clearfix"><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="__info">
                <p class="__category font-serif-italic fz-6-s">Women / Clothe</p><a href="#" class="font-heading fz-6-ss">SKATER DRESS IN LEAF</a>
                <div class="clearfix">
                  <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div><span class="__price color-primary font-heading">$280</span>
                </div>
              </div>
            </div>
            <div class="block-shop-product-small">
              <div class="__image overlay-container"><img src="assets/images/shop/product-img-17-s.jpg" alt="Shop Product"/>
                <div class="overlay-hover text-center bgc-dark-o-3">
                  <ul>
                    <li class="clearfix"><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="__info">
                <p class="__category font-serif-italic fz-6-s">Women / Clothe</p><a href="#" class="font-heading fz-6-ss">SKATER DRESS IN LEAF</a>
                <div class="clearfix">
                  <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div><span class="__price color-primary font-heading">$280</span>
                </div>
              </div>
            </div>
            <div class="block-shop-product-small">
              <div class="__image overlay-container"><img src="assets/images/shop/product-img-23-s.jpg" alt="Shop Product"/>
                <div class="overlay-hover text-center bgc-dark-o-3">
                  <ul>
                    <li class="clearfix"><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="__info">
                <p class="__category font-serif-italic fz-6-s">Women / Clothe</p><a href="#" class="font-heading fz-6-ss">SKATER DRESS IN LEAF</a>
                <div class="clearfix">
                  <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div><span class="__price color-primary font-heading">$280</span>
                </div>
              </div>
            </div>
            <div class="block-shop-product-small">
              <div class="__image overlay-container"><img src="assets/images/shop/product-img-24-s.jpg" alt="Shop Product"/>
                <div class="overlay-hover text-center bgc-dark-o-3">
                  <ul>
                    <li class="clearfix"><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="__info">
                <p class="__category font-serif-italic fz-6-s">Women / Clothe</p><a href="#" class="font-heading fz-6-ss">SKATER DRESS IN LEAF</a>
                <div class="clearfix">
                  <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div><span class="__price color-primary font-heading">$280</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="widget widget-tag">
          <h6 class="__widget-title">POPULAR TAGS</h6>
          <div class="__widget-content tags"><a href="#">Dress</a><a href="#">Mini</a><a href="#">Skate animal</a><a href="#">Lorem ipsum</a><a href="#">litterature</a><a href="#">Dress</a><a href="#">Mini</a><a href="#">Skate animal</a><a href="#">Lorem ipsum</a><a href="#">litterature</a><a href="#">Dress</a><a href="#">Mini</a><a href="#">Skate animal</a>
          </div>
        </div>
      </aside>
      <nav class="offcanvas-nav bgc-gray-darkest"><a href="#" class="offcanvas-nav-toggle"><i class="icon-wrong6"></i></a>
        <h6 class="size-s smb">CUSTOM PAGES</h6>
        <nav class="nav-single nav-item">
          <ul>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Portfolio</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h6 class="size-s smb">ADDTIONAL LINKS</h6>
        <nav class="nav-single nav-item">
          <ul>
            <li><a href="#">Retina Homepage</a></li>
            <li><a href="#">New Page Examples</a></li>
            <li><a href="#">Parallax Sections</a></li>
            <li><a href="#">Shortcode Central</a></li>
            <li><a href="#">Ultimate Font</a></li>
            <li><a href="#">Collection</a></li>
          </ul>
        </nav>
        <h6 class="size-s">GALLERY</h6>
        <div class="widget widget-gallery">
          <ul class="__widget-content magnific-gallery">
            <li><a href="assets/images/vintage/vintage-1.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-1-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-2.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-2-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-3.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-3-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-4.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-4-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-5.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-5-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-6.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-6-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-7.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-7-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-8.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-8-thumbnail.jpg" alt="instagram image"/></a></li>
            <li><a href="assets/images/vintage/vintage-9.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-9-thumbnail.jpg" alt="instagram image"/></a></li>
          </ul>
        </div>
        <ul class="social circle gray">
          <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
          <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
          <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
          <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
        </ul>
      </nav>


      <div class="mobile-nav"><a href="#" class="mobile-nav-toggle"><i class="icon-close47"></i></a><a href="index.php" class="megatron">
          <div class="logo">
            <svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
          </div>
          <div class="brand">MEGATRON</div></a>
        <nav class="os-menu mobile-menu">
          <ul>
            <li><a href="index.php">HOME</a>
            </li>
            <li><a href="about.php">ABOUT US</a>
            </li>
            <li><a href="category.php">RENTAL</a>
            </li>
            <li><a href="category.php">SALE</a>
            </li>
            <li><a href="contact.php">CONTACT US</a>
            </li>
          </ul>
        </nav>
        <div class="search-area">
          <form method="post" class="search-box">
            <input type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-icon"></i></button>
          </form>
        </div>
        <div class="social-area">
          <ul class="social circle gray">
            <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
            <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
            <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
            <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
          </ul>
        </div>
      </div>
      <!-- Page Navigation-->
      <!--Page Header-->
      <header class="page-header small common-header bgc-dark-o-5">
        <div data-parallax="scroll" data-position="top" data-image-src="assets/images/background/background-1.jpg" data-speed="0.3" class="parallax-background"></div>
        <div class="container text-center cell-vertical-wrapper">
          <div class="cell-middle">
            <h1 class="text-responsive size-l nmb">MEGATRON SHOP</h1>
            <p class="common-serif text-responsive">Shop from over 850 of the best brands</p>
          </div>
        </div>
        <div class="ab-bottom col-xs-12">
          <div class="container">
            <div class="breadcrumb bgc-dark-o-6"><span class="__title font-heading fz-6-s">YOU ARE HERE :</span><span class="__content italic font-serif fz-6-ss"><span><a href="index.html">Home</a></span><span><a href="#">Store</a></span><span><a href="#">Fullwidth</a></span></span></div>
          </div>
        </div>
      </header>
      <!--End Page Header-->
      <!--Page Body-->
      <div id="page-body" class="page-body">
        <section class="page-section">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 hidden-lg">
                <aside class="shop-category-sidebar">
                  <div class="widget widget-search">
                    <div class="__widget-content">
                      <form method="post" class="search-box">
                        <input type="search" placeholder="Search..."/>
                        <button type="submit"><i class="icon-search-icon"></i></button>
                      </form>
                    </div>
                  </div>
                  <div class="widget widget-categories">
                    <h6 class="__widget-title">CATEGORIES</h6>
                    <ul class="__widget-content">
                      <li><a href="#">DESTINATION (8)</a></li>
                      <li><a href="#">FOOD (8)</a></li>
                      <li><a href="#">LANDSCAPE (8)</a></li>
                      <li><a href="#">NATURE (8)</a></li>
                      <li><a href="#">PHOTOGRAPHY (8)</a></li>
                      <li><a href="#">TRAVEL (8)</a></li>
                    </ul>
                  </div>
                  <hr class="mb-40">
                  <div class="widget widget-price-filter">
                    <h6 class="__widget-title">FILTER BY PRICE</h6>
                    <div data-from="5" data-to="150" data-min="0" data-max="500" class="__widget-content price-filter">
                      <div class="price-slider-range"></div>
                      <div class="price-filter-detail">
                        <div class="price pull-left">Price :&nbsp;&pound;<span class="from"></span><span class="hyphen">&nbsp;-&nbsp;&pound;</span><span class="to"></span></div><a href="#" class="pull-right btn btn-small btn-primary">FILTER</a>
                      </div>
                    </div>
                  </div>
                </aside>
              </div>
            </div>
            <div class="row product-listing">
              <div class="col-xs-12">
                <div class="filter-and-sort type-1 clearfix">
                  <div class="__content-left">
                    <div class="__hamburger shop-sidebar-toggle"><span></span></div>
                    <p class="font-heading fz-6-s">FILTER</p>
                  </div>
                  <div class="__content-right category-sorting clearfix">
                    <div data-menu-class="__small" class="select-wrapper __small">
                      <select name="orderby" class="select-menu">
                        <option value="menu_order">Default Sorting</option>
                        <option value="popularity">Sort By Popularity</option>
                        <option value="rating">Sort By Average Rating</option>
                        <option value="date">Sort By Newness</option>
                        <option value="price">Sort By Price: Low To High</option>
                        <option value="price-desc">Sort By Price: High To Low</option>
                      </select>
                    </div>
                    <p class="font-heading fz-6-s">Showing&nbsp;<span class="from">13</span>-<span class="to">24</span>&nbsp;of&nbsp;<span class="total">50</span>&nbsp;results</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><img src="assets/images/shop/product-img-1-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status sale-off">SALE</span><img src="assets/images/shop/product-img-2-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price">
                      <del>$250</del><span>$230</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status new">NEW</span><img src="assets/images/shop/product-img-3-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status sale-off">SALE</span><img src="assets/images/shop/product-img-4-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price">
                      <del>$240</del><span>$210</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><img src="assets/images/shop/product-img-5-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status new">NEW</span><img src="assets/images/shop/product-img-6-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status hot">HOT</span><img src="assets/images/shop/product-img-7-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price">
                      <del>$300</del><span>$210</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><img src="assets/images/shop/product-img-8-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><img src="assets/images/shop/product-img-9-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="5" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status hot">HOT</span><img src="assets/images/shop/product-img-21-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><img src="assets/images/shop/product-img-11-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="5" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                <div class="block-shop-product font-heading">
                  <div class="__image overlay-container"><span class="status new">NEW</span><img src="assets/images/shop/product-img-12-m.jpg" alt="Shop Product"/>
                    <div class="overlay text-center">
                      <div class="__layer bgc-dark-o-3"></div>
                      <ul>
                        <li class="clearfix"><a href="#" class="compare"><i class="icon-185061-photo-pictures-streamline"></i></a><a href="#shop-quickview" data-modal-open="data-modal-open" class="quick-view"><i class="icon-search-icon"></i></a><a href="shop-cart.html" class="add-to-cart"><i class="icon-shopping111"></i></a><a href="#" class="wish-list"><i class="icon-185083-like-love-streamline"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info text-center"><a href="#">FITCH ZIP THROUGH HOODIE</a>
                    <div class="__price"><span>$200</span>
                    </div>
                    <div data-rating="4" class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="page-section no-padding border-bottom-lighter border-top-lighter">
          <div class="container">
            <nav class="pagination-2"><a href="#" class="button-previous"><i class="icon-left-open-big"></i><span>PREVIOUS</span></a><a href="#" class="button-next"><i class="icon-right-open-big"></i><span>NEXT</span></a>
              <ul class="text-center">
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
              </ul>
            </nav>
          </div>
        </section>
        <section class="page-section">
          <div class="group-shop-banner">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 text-center __header">
                  <p class="font-serif-italic fz-3">For every blanket you purchase, we will give a blanket to your local homeless shelter.</p>
                  <hr class="hr-gray-dark"/>
                </div>
                <div data-wow-delay="0.3s" class="col-sm-6 col-xs-12 section-block wow fadeInUp">
                  <div class="block-shop-banner"><a href="#">
                      <div style="background-image:url('assets/images/banner/shop-banner-3.jpg')" class="__content overlay-container">
                        <div class="overlay text-center">
                          <div class="cell-vertical-wrapper">
                            <div class="cell-middle">
                              <h3 class="mb-10">NOW UP TO AN EXTRA</h3>
                              <h1 class="fz-1-l mb-5">50% OFF</h1>
                              <p class="mb-0 font-serif-italic fz-4">Deep 2015 Fall Delivery</p>
                            </div>
                          </div>
                        </div>
                      </div></a></div>
                </div>
                <div data-wow-delay="0.3s" class="col-sm-6 col-xs-12 section-block wow fadeInUp">
                  <div class="block-shop-banner"><a href="#">
                      <div style="background-image:url('assets/images/banner/shop-banner-4.jpg')" class="__content overlay-container">
                        <div class="overlay text-center">
                          <div class="cell-vertical-wrapper">
                            <div class="cell-middle">
                              <h3 class="mb-10">UP TO</h3>
                              <h1 class="fz-1-l mb-5">70% OFF</h1>
                              <p class="mb-0 font-serif-italic fz-4">Dress Desserts</p>
                            </div>
                          </div>
                        </div>
                      </div></a></div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <!--End Page Body-->
      <!--Page Footer-->
      <footer id="footer" class="page-footer footer-preset-5">
        <div class="footer-body bgc-gray-darker">
          <div class="container">
            <div class="row">
              <div class="col-lg-3 col-sm-6 col-xs-12 __block-wrapper-1">
                <div class="footer-widget-about"><a href="index.html" class="megatron inline size-2">
                    <div class="logo"><svg viewBox="0 0 100 100"> <g> <path d="M64.264,31.886v56.803l-14.319,8.259l-14.319-8.259v-74.3L14.157,26.771v41.271l7.156,4.128V30.905   l13.467-7.768v8.26l-6.311,3.64V84.56L7,72.178v-49.54L42.781,2v82.553l7.162,4.132l7.163-4.132V27.758L64.264,31.886z    M43.626,26.294l6.317-3.644L71.42,35.038V84.56l21.469-12.382v-49.54L57.106,2v15.546l7.157,4.128v-7.285l21.469,12.382v41.271   l-7.156,4.128V30.905L49.943,14.391l-6.317,3.644V26.294z"/> </g> </svg>
                    </div>
                    <div class="brand">MEGATRON</div></a>
                  <p>We are a creative company that specializes in strategy &amp; design. We like to create things with like - minded people who are serious about their passions. </p>
                  <ul class="social circle">
                    <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-flickr"></i></a></li>
                    <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6 col-xs-12 __block-wrapper-2">
                <div class="footer-widget-gallery">
                  <h4>FLICKR WIDGET</h4>
                  <div class="widget widget-gallery">
                    <ul class="__widget-content magnific-gallery">
                      <li><a href="assets/images/vintage/vintage-1.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-1-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-2.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-2-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-3.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-3-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-4.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-4-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-5.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-5-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-6.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-6-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-7.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-7-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-8.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-8-thumbnail.jpg" alt="instagram image"/></a></li>
                      <li><a href="assets/images/vintage/vintage-9.jpg" class="zoom-button img-wrapper"><img src="assets/images/vintage/vintage-9-thumbnail.jpg" alt="instagram image"/></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6 col-xs-12 __block-wrapper-3">
                <div class="footer-widget-recent-post">
                  <h4>RECENT POSTS</h4>
                  <div class="__content font-heading fz-6-ss"><span><a href="#">HISTORY OF BROOKLYN</a></span><span><a href="#">PENANG STREET FOOD</a></span><span><a href="#">THE MIST OF MADAGASCAR</a></span><span><a href="#">J.R.R. TOLKIEN</a></span><span><a href="#">MOROCCO ROAD TRIP</a></span><span><a href="#">THE NOISE - ELIXIR</a></span></div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6 col-xs-12 __block-wrapper-4">
                <div class="footer-widget-contact">
                  <h4>OUR OFFICE</h4>
                  <div class="__content font-heading fz-6-ss">
                    <div class="__address"><i class="icon icon-location"></i><span>14 Tottenham Road, London, England.</span></div>
                    <div class="__phone"><i class="icon icon-phone-1"></i><span>(123) 45678910</span></div>
                    <div class="__email"><i class="icon icon-paper-plane"></i><span>info@osthemes.com</span></div>
                    <div class="__phone-2"><i class="icon icon-mobile-1"></i><span>(123) 45678910</span></div>
                    <div class="__time"><i class="icon icon-clock-1"></i><span>Mon - Sat: 9:00 - 18:00</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-foot-3 bgc-gray-darkest">
          <div class="container">
            <div class="row">
              <div class="__content-left">
                <p class="font-heading fz-6-s">MEGATRON &copy; 2015 OSTHEMES - ENVATO PTY LTD</p>
              </div>
              <div class="__content-right">
                <nav class="footer-nav">
                  <ul class="font-heading">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">FEATURES</a></li>
                    <li><a href="#">PORTFOLIO</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="#">SHOP</a></li>
                    <li><a href="#">SHORTCODE</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!--End Page Footer-->

      
      <div id="shop-quickview" style="display:none;" class="shop-quick-view clearfix">
        <div class="row">
          <div class="col-sm-6 col-xs-12 product-thumbnail-slider-wrapper __content-left">
            <div class="product-thumbnail-slider">
              <div class="product-syn-slider-1-wrapper">
                <div class="product-syn-slider-1 syn-slider-1 direction-nav">
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-2-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-2-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-4-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-4-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-6-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-6-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-11-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-11-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-13-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-13-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-14-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-14-l.jpg" alt="Product slider image"/></div>
                  </div>
                </div>
              </div>
              <div class="product-syn-slider-2-wrapper">
                <div class="product-syn-slider-2 syn-slider-2">
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-2-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-4-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-6-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-11-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-13-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-14-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12 __content-right">
            <div class="product-detail">
              <h3 class="fz-3-l"><a href="shop-single-fullwidth.html">FITCH ZIP THROUGH HOODIE</a></h3>
              <div class="__rating clearfix">
                <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                <p class="color-secondary">(1 customer review)</p>
              </div>
              <div class="__price fz-4 font-heading">
                <del>$500</del><span class="color-primary">$300</span>
              </div>
              <div class="__text">
                <p>Nullam fringilla tristique elit id varius. Nulla lacinia quam nec venenatis dignissim. Vivamus volutpat tempus semper. Cras feugiat mi sit amet risus consectetur, non consectetur nisl finibus. Ut ac eros quis mi volutpat cursus vel non risus. In non neque lacinia, aliquet tortor sed, consectetur nibh. Nulla faucibus risus in ligula elementum bibendum.</p>
              </div>
              <form>
                <div class="__option-2 clearfix">
                  <div class="__quantity">
                    <div class="quantity-input">
                      <input type="text" value="1" class="number"/>
                      <button class="add icon-up-open-mini"></button>
                      <button class="subract icon-down-open-mini"></button>
                    </div>
                  </div>
                  <div class="__button"><a href="shop-cart.html" class="btn btn-primary"><i class="icon-shopping111"></i>ADD TO CART</a></div>
                </div>
              </form>
              <div class="__others">
                <p>SKU:&nbsp;<a href="#">0010</a></p>
                <p>CATEGORY:&nbsp;<a href="#">Other</a></p>
                <p>TAGS:&nbsp;<a href="#">Dress</a>,&nbsp;<a href="#">Fashion</a></p>
              </div>
              <ul class="social circle secondary responsive">
                <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                <li><a href="#"><i class="icon icon-stumbleupon"></i></a></li>
                <li><a href="#"><i class="icon icon-instagrem"></i></a></li>
                <li><a href="#"><i class="icon icon-dribbble-1"></i></a></li>
                <li><a href="#"><i class="icon icon-github"></i></a></li>
                <li><a href="#"><i class="icon icon-vimeo"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Page content-->
    <!--Javascript Library-->
    <button id="back-to-top-btn"><i class="icon-up-open-big"></i></button>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="vendors/waypoints/lib/shortcuts/sticky.min.js"></script>
    <script src="vendors/smoothscroll/SmoothScroll.js"></script>
    <script src="vendors/wow/dist/wow.min.js"></script>
    <script src="vendors/parallax.js/parallax.js"></script>
    <script type="text/javascript" src="vendors/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="vendors/slick-carousel/slick/slick.js"></script>
    <script type="text/javascript" src="vendors/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="vendors/jquery-modal/jquery.modal.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <!-- Google analytics-->
    <script type="text/javascript">(function(b,o,i,l,e,r){b.GoogleAsnalyticsObject=l;b[l]||(b[l]=function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;e=o.createElement(i);r=o.getElementsByTagName(i)[0];e.src='//www.google-analytics.com/analytics.js';r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));ga('create','UA-57387972-3');ga('send','pageview');</script>
    <!--End Javascript Library-->
  </body>
</html>